Milestone 1 Decription 

The project's goal is to learn to work with Harmony and FreeRTOS to create 
message queues, communicate through GPIO and UART Pins and read values on the 
logic analyzer. 

How To Run Project

1- Clone the project
    Note: Harmony Configuration must be similar for the project to work
    
2- Set up the board, the LA Pins, the UART Pin and IR Sensor

    LA:
    
        Lines 0-8 are set on pins 37-30, Lines 10-17 are set on pins 46-53
        
        UART is set to pin 18 on the board
        
        Black Pins are set to ground
        
    IR Sensor:
    
        5V - 3.3V power
        
        GND - Ground
        
        Ain - pin A0 
        
3- Power the board and connect it to a pickit3

4- Flash the board 

5- Run on DigiView with the correct pin configuration


