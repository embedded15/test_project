/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "debug.h"

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */
/** Descriptive Data Item Name

  @Summary
    Brief one-line summary of the data item.
    
  @Description
    Full description, explaining the purpose and usage of data item.
    <p>
    Additional description in consecutive paragraphs separated by HTML 
    paragraph breaks, as necessary.
    <p>
    Type "JavaDoc" in the "How Do I?" IDE toolbar for more information on tags.
    
  @Remarks
    Any additional remarks
 */
int global_data;


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** */



/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

// *****************************************************************************

/** 
  @Function
    int ExampleInterfaceFunctionName ( int param1, int param2 ) 

  @Summary
    Brief one-line description of the function.

  @Remarks
    Refer to the example_file.h interface header for function usage details.
 */
void EventGPIODisplay(unsigned char outVal) {
    /* Pin 46 - Corresponds to bit 0*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_F, PORTS_BIT_POS_1, (outVal & 0x01));
    /* Pin 47 - Corresponds to bit 1*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_6, (outVal & 0x02) >> 1);
    /* Pin 48 - Corresponds to bit 2*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_8, (outVal & 0x04) >> 2);
    /* Pin 49 - Corresponds to bit 3*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_D, PORTS_BIT_POS_11,(outVal & 0x08) >> 3);
    /* Pin 50 - Corresponds to bit 4*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_7, (outVal & 0x10) >> 4);
    /* Pin 51 - Corresponds to bit 5*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_8, (outVal & 0x20) >> 5);
    /* Pin 52 - Corresponds to bit 6*/
    PLIB_PORTS_PinWrite(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_6, (outVal & 0x40) >> 6);
    /* Pin 53 - Corresponds to bit 7 which will be toggled*/
    PLIB_PORTS_PinToggle(PORTS_ID_0, PORT_CHANNEL_G, PORTS_BIT_POS_9);
    
    
}
void ValueGPIODisplay(unsigned char outVal) {
      
    PLIB_PORTS_Write(PORTS_ID_0, PORT_CHANNEL_E, outVal);
}
void dbgOutputVal(unsigned char outVal, unsigned char centi, unsigned char meter) {
    if(outVal <= 127) {
        // Output to 8 I/O Pins
        //PORTE = PORTE ^ 0x80; /* Toggling the eighth Bit */
        ValueGPIODisplay(outVal);
        ValueGPIODisplay(centi);
        ValueGPIODisplay(meter);
    }
}

void dbgUARTVal(unsigned char outVal, unsigned char centi, unsigned char meter) {
    // Output the average of every four sensor messages
        while(PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, outVal);
        while(PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, centi);
        while(PLIB_USART_TransmitterBufferIsFull(USART_ID_4));
    /* Send one byte */
    PLIB_USART_TransmitterByteSend(USART_ID_4, meter);
    
}

void dbgOutputEvent(unsigned char outVal) {
    // outVal corresponds to an event
    // outVal is a unique value for a specific event
    if (outVal == EVENT_FAIL) {stopEverything();}
    if(outVal <= 127) {
        // Output to a different 8 I/O pins
        // Pins need to be accessible when used with other boards
        //DBG_EVENT_OUT(outVal);
        EventGPIODisplay(outVal);
    }
}
void stopEverything() {
    // Stop everything from running and end the program
    /************************************************************************
     * Disable Interrupts
     ************************************************************************/
    SYS_INT_StatusGetAndDisable();
    DRV_ADC_Stop();
    
    /************************************************************************
     * Disable Suspend All Threads
     ************************************************************************/
    while(1) {
        //DBG_EVENT_OUT(EVENT_FAIL);
        EventGPIODisplay(EVENT_FAIL);
        vTaskSuspend( NULL );
    }
     
    // Enter infinite while loop 
    // Then send out that the event failed

}

/* *****************************************************************************
 End of File
 */
