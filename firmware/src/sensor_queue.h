/* ************************************************************************** */
/** Descriptive File Name


  @File Name
    sensor_queue.h

  @Summary
 file containing subroutines that create/read/write the queue
  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#ifndef _SENSOR_QUEUE_H    /* Guard against multiple inclusion */
#define _SENSOR_QUEUE_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "debug.h"
#include "FreeRTOS.h"
#include "queue.h"
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */

    /*  A brief description of a section can be given directly below the section
        banner.
     */

#define MSG_TYPE            uint8_t
#define MSG_QUEUE_LENGTH    10

    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************

    QueueHandle_t sensorMsgQueue;
    // *****************************************************************************
    // Section: Interface Functions
    // *****************************************************************************

    /*  A brief description of a section can be given directly below the section
        banner.
     */

    // *****************************************************************************
    /***/
    EVENT_TYPE appCreateSensorMsgQueue();
    EVENT_TYPE appSendMsgQueue(void* message);
    EVENT_TYPE appReceivefromQueue(void* message);
    EVENT_TYPE ISRSendMsgtoQueue(void* message, portBASE_TYPE*pxHigherPriorityTaskWoken);

#endif /* _SENSOR_QUEUE_H */

/* *****************************************************************************
 End of File
 */
