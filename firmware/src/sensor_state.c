/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    sensor_state.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "sensor_state.h"

/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

/* ************************************************************************** 
 * FourStatesMachine(unsigned char receivedMsg) 
 * 
 * Made up of four states in which each state receives a value from the sensor, outputs
 * it to GPIO, and it averages the value received and outputs it to the UART
 * 
 ********************************************************************************/
int fourStatesMachine(unsigned char receivedMsg, unsigned char centi, unsigned char meter) {
    switch(state) {
        case FIRST_VAL:
            dbgOutputVal(receivedMsg, centi, meter);           /* output the value to GPIO pins */
            dbgOutputEvent(DISPLAYED_ON_GPIO);
            avgSensorValue += (unsigned int) receivedMsg;       /* Sums the first value to finally be divided by 4*/
            state = SECOND_VAL;                                 /* Transition to the second state of receiving the second value */
            dbgOutputEvent(FIRST_STATE_DONE);
            break;
        case SECOND_VAL:
            dbgOutputVal(receivedMsg, centi, meter);           /* outputs the value to GPIO*/
            dbgOutputEvent(DISPLAYED_ON_GPIO);
            avgSensorValue += (unsigned int) receivedMsg;                /* sums the second value to then be averaged */
            state = THIRD_VAL;                                  /* transition to the third state to receive the third value */
            dbgOutputEvent(SECOND_STATE_DONE);
            break;
        case THIRD_VAL:
            dbgOutputVal(receivedMsg, centi, meter);           /* outputs the value to GPIO */
            dbgOutputEvent(DISPLAYED_ON_GPIO);
            avgSensorValue += (unsigned int) receivedMsg;       /* sums the third value to then be averaged */
            state = FOURTH_VAL_AND_AVG;                         /* transition to the fourth value to receive the last value and find the average */
            dbgOutputEvent(THIRD_STATE_DONE);
            break;
        case FOURTH_VAL_AND_AVG:
            dbgOutputVal(receivedMsg, centi, meter);           /* output the value to GPIO */
            dbgOutputEvent(DISPLAYED_ON_GPIO);
            avgSensorValue += (unsigned int) receivedMsg;       /* Sums the average then averages it */
            avgSensorValue = avgSensorValue >> 2;
            // OUTPUT TO UART
            dbgUARTVal((unsigned char) avgSensorValue, centi, meter);         /* Output the value to UART */
            dbgOutputEvent(DISPLAYED_ON_UART);
            avgSensorValue = 0;                                 /* reset the value to 0 */
            state = FIRST_VAL;                                  /* transition to the first state */
            dbgOutputEvent(FOURTH_STATE_DONE);
            break;
        default:
            stopEverything();                                   /* If none of the states work then there is a fatal error in which all processes must be stopped */
            break;
            
    }
}

/* *****************************************************************************
 End of File
 */
