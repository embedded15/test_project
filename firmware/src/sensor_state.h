/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    sensor_state.h

  @Summary
 Sensor state machine that outputs values to output peripherals (UART and GPIO Pins)

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _SENSOR_STATE_H    /* Guard against multiple inclusion */
#define _SENSOR_STATE_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */
#include "sensor_queue.h"
#include "debug.h"


/* Enum that defines the states of the state machine
 * FIRST_VAL - Receives the first value and outputs it to GPIO
 * SECOND_VAL - Receives the second value and outputs it to GPIO
 * THIRD_VAL - Receives the third value and outputs it to GPIO
 * FOURTH_VAL - Receives the fourth value and outputs it to GPIO then
 *              averages the four values and outputs it to UART
 */
    typedef enum {
        FIRST_VAL, SECOND_VAL, THIRD_VAL, FOURTH_VAL_AND_AVG
    }SENSOR_STATE;
    
    SENSOR_STATE state = FIRST_VAL;
    int avgSensorValue = 0; /* Initially the values are 0 */
    // *****************************************************************************
    // *****************************************************************************
    // Section: Interface Functions
    // *****************************************************************************
    // *****************************************************************************

    /*****************************************************************************
     * four state machine
     * 
     * It is called in app.c app_task function.
     * 
     * It receives a value from the message queue that was passed
     * from an ADC sensor 
     * Then it outputs the values in 8 GPIO pins or UART
     *****************************************************************************/

    int fourStatesMachine(unsigned char receivedMsg, unsigned char centi, unsigned char meter);


#endif /* _SENSOR_STATE_H */

/* *****************************************************************************
 End of File
 */
