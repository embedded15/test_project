/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// Section: Included Files 
// *****************************************************************************

#include "app.h"
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
    // Create the Message Queue
    
    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
    /////////////////////////////////////////////////////////////
    // INITIALIZE freeRTOS queue
    /////////////////////////////////////////////////////////////
    // Sensor Queue
    
    DRV_TMR0_Initialize();
    PLIB_INT_SourceFlagClear(INT_ID_0, INT_SOURCE_TIMER_2);
    PLIB_INT_SourceEnable(INT_ID_0, INT_SOURCE_TIMER_2);
    PLIB_TMR_Start(TMR_ID_2);
    //PLIB_INT_SourceEnable(INT_ID_0, INT_SOURCE_TIMER_2);
    dbgOutputEvent(appCreateSensorMsgQueue());
    dbgOutputEvent(CREATED_MSG_QUEUE);
    // Initialize the events
    DRV_ADC_Open();
    DRV_ADC_Start();
    usartHandle = DRV_USART_Open(DRV_USART_INDEX_0, DRV_IO_INTENT_WRITE);
    if (usartHandle == DRV_HANDLE_INVALID) {
        // Register EVENT handler for write
        ////////////////////////////////////////////
        // I THINK OUR PROGRAM STOPS HERE/
        //////////////////////////////////////////////////
       stopEverything();
    }
      
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks(void) {
    dbgOutputEvent(TASK_BEGIN);
    while(1) {
                dbgOutputEvent(TASK_IN_INFINITE_LOOP);
                dbgOutputEvent(TASK_RECEIVING_FROM_QUEUE);
                // Subroutine that reads from message queue
                appReceivefromQueue(&app.msg);
                MSG_TYPE c;
                MSG_TYPE m;
                dbgOutputEvent(appReceivefromQueue(&c));
                dbgOutputEvent(appReceivefromQueue(&m));
                dbgOutputEvent(TASK_RECEIVED_FROM_QUEUE);
                //Send the debug Message
                // Subroutine that implements a 4 state machine
                fourStatesMachine(app.msg, c, m);
        // Receive Data
    }
}

 

/*******************************************************************************
 End of File
 */
