/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    sensor_data.h

  @Summary
 Sensor data are Analog values that will be converted to Digital values through an ADC
 * conversion. This conversion is made in this file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _SENSOR_DATA_H    /* Guard against multiple inclusion */
#define _SENSOR_DATA_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "debug.h"
#include "sensor_queue.h"
/* Provide C++ Compatibility */
    /* ************************************************************************** */
  

    // *****************************************************************************
    // *****************************************************************************
    // Section: Interface Functions
    // *****************************************************************************
    // *****************************************************************************

    /*  A brief description of a section can be given directly below the section
        banner.
     */
/*****************************************************************************
     * sendDataToMsgQueue
     * function in which the ADC Converter is called and the distance is sent to the msg queue
     *****************************************************************************/
    void sendDataToMsgQueue();
    /*****************************************************************************
     * ADC CONVERTER
     * The analog data is passed in to the function and the function returns a 
     * 10 bit data that only makes sense when converted
     * Then it sends the data to the msg queue
     *****************************************************************************/
    
    unsigned int ADCConverter(int analogData);




#endif /* _SENSOR_DATA_H */

/* *****************************************************************************
 End of File
 */
