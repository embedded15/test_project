/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    debug.h

  @Summary
 This file will 

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _DEBUG_H    /* Guard against multiple inclusion */
#define _DEBUG_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */
#include "peripheral/ports/plib_ports.h"
#include "driver/usart/drv_usart.h"
//#include "app.h"

    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */

    /*  A brief description of a section can be given directly below the section
        banner.
     */


    /* ************************************************************************** */
    /** Descriptive Constant Name

      @Summary
        Brief one-line summary of the constant.
    
      @Description
        Full description, explaining the purpose and usage of the constant.
        <p>
        Additional description in consecutive paragraphs separated by HTML 
        paragraph breaks, as necessary.
        <p>
        Type "JavaDoc" in the "How Do I?" IDE toolbar for more information on tags.
    
      @Remarks
        Any additional remarks
     */
/* Define Different Constants that represent specific Events */
#define EVENT_TYPE          unsigned char
#define EVENT_FAIL          0xFF
#define EVENT_SUCCESS       0x1
/*****************************************************************************
 * TASK EVENT NUMBERS
 *****************************************************************************/
#define TASK_BEGIN                  0x60
#define TASK_IN_INFINITE_LOOP       0x61
#define TASK_RECEIVING_FROM_QUEUE   0x62
#define TASK_RECEIVED_FROM_QUEUE    0x63
#define CREATED_MSG_QUEUE           0x64

/*****************************************************************************
 * DISPLAY EVENTS
 *****************************************************************************/
#define DISPLAYED_ON_GPIO           0x6A
#define DISPLAYED_ON_UART           0x6B

/*****************************************************************************
 * STATES EVENTS
 ****************************************************************************/
#define FIRST_STATE_DONE            0xA
#define SECOND_STATE_DONE           0xB
#define THIRD_STATE_DONE            0xC
#define FOURTH_STATE_DONE           0xD


/*****************************************************************************
 * ISR EVENT NUMBERS
 *****************************************************************************/
#define ENTER_ISR              0x70
#define LEAVING_ISR            0x71
#define ISR_SENDING_TO_QUEUE   0x72
#define ISR_SENT_TO_QUEUE      0x73

    /*
     * FOUR SUBROUTINE FOR DEBUGGING PURPOSES
     */
    /***************************************************************************
     * Debug Initialize
     * Here we initialize the pins that we want to configure
     ***************************************************************************
    /***************************************************************************
     * Debug Value Out to GPIO
     * Here we output event to the pins
     ***************************************************************************/
    void ValueGPIODisplay(unsigned char outVal); 
    /***************************************************************************
     * Debug Event Out to GPIO
     * Here we output event to the pins
     ***************************************************************************/
    void EventGPIODisplay(unsigned char outVal);
    /***************************************************************************
     * Debug Output Value
     * Here we output to the 8 I/O pins
     * Verify that the value of outputVal is less than or equal to 127
     ***************************************************************************/
    void dbgOutputVal(unsigned char outVal, unsigned char centi, unsigned char meter);
    /***************************************************************************
     * Debug UART Value
     * Value Written to UART
     * Verify that the value of outputVal is less than or equal to 127
     ***************************************************************************/
    void dbgUARTVal(unsigned char outVal, unsigned char centi, unsigned char meter);
    /***************************************************************************
     * STOP EVERYTHING ROUTINE
     * This routine makes sure everything stops working 
     ***************************************************************************/
    void dbgOutputEvent(unsigned char outVal);
    void stopEverything();
#endif /* _DEBUG_H */

/* *****************************************************************************
 End of File
 */
