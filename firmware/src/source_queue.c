/* ************************************************************************** */
/** Descriptive File Name


  @File Name
    sensor_queue.c

  @Summary
 file containing subroutines that create/read/write the queue and their implementations
  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#include "sensor_queue.h"

/******************************************************************************
 * Create the Message Queue
 * Description: 
 * Message Queue routine from RTOS API is called with a defined queueSize
 * In this case, each queue block is the size of a 32 bit integer type.
 ******************************************************************************/
EVENT_TYPE appCreateSensorMsgQueue() {
    /**************************************************************************
     * xQueueCreate 
     * 
     *************************************************************************/
    sensorMsgQueue = xQueueCreate(MSG_QUEUE_LENGTH, sizeof(MSG_TYPE));
    
    if(sensorMsgQueue == NULL) {
        // Send Error Message due to failure
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
/*******************************************************************************
 * NOT CALLED BY ISR
 * Send Message to Queue  
 * message is the message to be sent to the queue
 * returns nothing
 * waits until successfully sent to queue
 *******************************************************************************/
EVENT_TYPE appSendMsgtoQueue(void* message) {
    /* 
     * @sensorMsgQueue is the sensor queue which is not called in the ISR
     * the message needs to be sent as a void pointer 
     * portMAX_DELAY means that we wait indefinitely for a message
     * Thus this is a blocking call
     */
    
    // If the queue was not able to send a message, then an error has occured
    if(xQueueSend(sensorMsgQueue,  message, (TickType_t) 0) != pdPASS) {
        // Handle the error
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}

/*******************************************************************************
 * NEVER CALL IN ISR
 * IT is a blocking call to receive message from queue
 * Returns the value received from the queue
 *******************************************************************************/
EVENT_TYPE appReceivefromQueue(void* message) {
    if (xQueueReceive(sensorMsgQueue, message, portMAX_DELAY) != pdPASS) {
        // Hand Error
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
/*******************************************************************************
 * ISR Call to Send message to Queue
 * 
 *******************************************************************************/
EVENT_TYPE ISRSendMsgtoQueue(void* message, portBASE_TYPE* pxHigherPriorityTaskWoken) {
    // pdFALSE means we have not used the ISR for anything yet
    if(xQueueSendToBackFromISR(sensorMsgQueue, message, pxHigherPriorityTaskWoken) != pdPASS) {
        return EVENT_FAIL;
    }
    return EVENT_SUCCESS;
}
/*******************************************************************************
 * ISR Routine to receive message from queue
 *******************************************************************************/